<!-- TODO: This should be the code that creates the top bar and then the navigation bar. The navigation bar code should load the active tab so there should likely be another reference to the bit that loads the tab code. That _should_ turn into something that a servlet hands me but for now it will be JSP -->
			<div class="yui-d3f" id="mainPage">
<!-- Within the header include there's also center left and right menu items that are probably appropriate for their own includes. -->
<div id="header">
	<div id="menubar">
		<div id="left-menu-items">
		<!-- TODO: Put something appropriate here -->
			<span id="welcome" class="rborder">Welcome, Aaron</span>
			<!-- TODO: Come up with a good way to hide the stuff I don't want shown yet that doesn't involve a ton of oddly placed comments -->
			<a href="/pdtview/upload" title="Upload QKView" class="rborder">Upload</a>
			<a id="adminOpener" href="#" class="rborder">Options...</a>
<div id="adminDialog" class="qv-dialog" title="Options"> <!-- This deifnes a little dialog that pops up and lets you impersonate another user -->
	<div class="options">
		<ul>
			<li id="switch-block" class="form">
			<!-- TODO: Get rid of this switchuser thing but put something else here if appropriate -->
				<label for="switchUser">Impersonate user</label> 
				<form action="/pdtview/impersonation/start" method="post" name="switch">
					<div id="impersonate">
						<input id="switchUser" name="switchUser" type="text" class="required email" maxlength="100" style="display:block"/>
					</div>
					<div id="feedback" class="feedback">
						<label for="switchUser" class="error" generated="true"></label>
					</div>
					<div>
						<button id="switchButton" type="button" class="button switch">Switch</button>
					</div>
				</form>
			</li>
		</ul>
	</div> <!-- close cass="options" -->		
</div> <!-- close adminDialog -->
		<a href="/pdtview/logoff" title="Log out of PDT Viewer">Log out</a>
		</div> <!-- close left-menu-items -->
		<div id="center-menu-items">
		</div>
		<div id="right-menu-items">
		<!-- TODO: Update this -->
			<a class="rborder" href="/pdtview/releaseNotes" title="View BIG-IP iHealth Release Notes" target="new">What's new?</a>
			<a id="feedbackOpener" href="#" class="rborder">Feedback</a>
<div id="feedbackDialog" class="qv-dialog" title="Feedback">
	<div id="feedbackForm">
	<!-- TODO: Get rid of or replace feedback form -->
		<form action="/pdtview/feedback/new" method="post">
			<p>
				This form is for iHealth or diagnostics related feedback only.  If you are
				encountering issues with F5 equipment in your network, please contact F5&nbsp;Support
				via one of the methods listed 
				<a href="http://support.f5.com/kb/en-us/solutions/public/2000/600/sol2633.html" target="_new"=>here</a>.
			</p> 
			<div id="feedbackFormContent">
				<dl>
					<dt>From</dt>
						<dd>a.forster@f5.com</dd>
					<dt>To</dt>
						<dd>BIG-IP iHealth team</dd>
					<dt>Subject</dt>
						<dd>BIG-IP iHealth Viewer application</dd>
				</dl>
				<div class="message-block">
					<label for="feedbackMessage">Message</label>
					<div>
						<input id="qkviewId" name="qkviewId" type="hidden" />
						<textarea id="feedbackMessage" name="feedbackMessage" cols="52" rows="9" class="required" maxlength="2000" style="display:block"></textarea>
					</div>
				</div>
				<div>
					<button id="send" type="button" class="button switch">Send</button>
				</div>
			</div> <!-- close feedbackFormContent -->
			<div class="feedback"></div>
		</form>
	</div> <!-- close feedbackForm -->
	<div id="thankYou">
		<div id="thankYouMessage">
			Thank you for providing your feedback.  Your submission has been sent to F5 for evaluation. 
		</div>
		<div>
			<button id="close" type="button" class="button switch">Close</button>
		</div>
	</div> <!--  close thankYou -->		
</div> <!-- close feedbackDialog -->
			<a href="#" class="rborder">Help</a>
			<div id="help-submenu" class="submenu">
				<ul> 
					<li><a href="http://support.f5.com/kb/en-us/products/big-ip_ltm/manuals/related/bigip_ihealth_user_guide.html" target="_new">User&nbsp;Guide</a></li>
					<li><a href="/pdtview/about">About</a></li>
				</ul>
			</div>
			<a href="#">F5</a>
			<div id="f5-submenu" class="submenu">
				<ul> 
					<li><a href="http://www.f5.com" target="_new">F5&nbsp;Home</a></li>
					<li><a href="http://support.f5.com/" target="_new">AskF5</a></li>
					<li><a href="http://downloads.f5.com/" target="_new">Downloads</a></li>
				</ul>
			</div> <!-- close f5-submenu -->
		</div> <!-- close right-menu-items -->
	</div>  <!-- close menubar -->
	<div id="banner">
		<div id="logo-and-title">
			<div id="logo">
				<a href="/pdtview/"><img alt="F5 Logo" src="/pdtview/assets/images/logo_f5.png"></a>
			</div>
			<div id="logo-title">
				<h1><a href="/pdtview/">Portable Diagnostics Tool Viewer</a></h1>
			</div>
		</div>
		<div id="message">
			<div class="close"></div>
			<div class="count"></div>
			<div id="messagetype" class="type request">
				<div class="indicator1"></div><div class="indicator2"></div>
				<div id="messagetitle">Request...</div>
				<div id="messagetext">Receiving data from the PDT service.</div>
			</div>  <!-- close messagetype -->
		</div> <!-- close message -->
	</div>  <!-- close banner -->
</div> <!-- close Header -->



<!-- Begin Main Body -->
<!-- I think 'bd' is short for 'body' which is stupid, be less obscure -->
<div id="bd">
	<div class="yui-g home-page">
	<!-- Begin Navigation Header -->
<div id="maintabs">
    <ul class="ui-tabs-nav">
    	<!-- The first page will just be a text box where you paste in the xml and it presents it in a navicable format. -->
    	<li><a href="#tabs-1">Visualize a PDT output file</a></li>
    	<!-- Later you will be able to upload the files and store them to have historical and potentially comparisons -->
    	<!-- Eventually I'd like to be able to have users upload a QKView and then we will submit it to PDTool and get the results back and give you navicable functionality like iHealth -->
        <li><a href="#tabs-2">My PDT Output files</a></li>
        <!-- TODO: See if I can do something like this -->
        <li><a href="#tabs-3">Saved Comparisons</a></li>
    </ul>
    <div id="tabs-1"><div id="myPDTResults" class="qv-results">
    <!-- <div id="tabs-1"><div id="myQkviews" class="qv-results"> -->
	<div class="fg-toolbar ui-widget-header ui-corner-all ui-helper-clearfix" style="padding: 3px" id="buttonsAndControls">
		<div class="buttons">
			<button class="upload-link">
				<span class="icon upload">Upload</span>
			</button>
				<button class="diff-link">
					<span class="icon diff">Compare</span>
				</button>
				<button class="hide-link">
					<span class="icon hide">Hide</span>
				</button>
		</div> <!-- end buttons -->
		<div class="search-controls">
			<div>
				<input id="showAllCB" type="checkbox" checked="checked" />Search All Users
			</div>
			<div>
				<input id="searchText" name="searchText" type="text" class="search" value="" />
			</div>
			<div>
				<button name="searchButton" id="searchButton">Search</button>
			</div>
		</div> <!-- end search-controls -->
	</div> <!-- end buttonsAndControls -->
<!-- End Navigation Header (sort of) This div isn't actually closed yet in the regular code the tabs change based on javaScript so that might change.-->