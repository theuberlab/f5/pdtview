<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<title>Portable Diagnostics Tool Viewer</title>
		<link rel="shortcut icon" type="image/x-icon" href="/pdtview/favicon.ico">
		<link rel="stylesheet" type="text/css" href="/pdtview/assets/application.css">
		<!--<script type="text/javascript" src="/pdtview/assets/application.js"></script>-->
	</head>
	<body>
		<div class="yui-d3f" id="mainPage">
<div id="hd">
	<div id="menubar">
		<div id="left-menu-items">
			<span id="welcome" class="rborder">Welcome, Aaron</span>
			<a href="/qkview-analyzer/upload" title="Upload QKView" class="rborder">Upload</a>
			<a id="adminOpener" href="#" class="rborder">Options...</a>
<div id="adminDialog" class="qv-dialog" title="Options">
	<div class="options">
		<ul>
			<li id="switch-block" class="form">
				<label for="switchUser">Impersonate user</label> 
				<form action="/qkview-analyzer/impersonation/start" method="post" name="switch">
					<div id="impersonate">
						<input id="switchUser" name="switchUser" type="text" class="required email" maxlength="100" style="display:block"/>
					</div>
					<div id="feedback" class="feedback">
						<label for="switchUser" class="error" generated="true"></label>
					</div>
					<div>
						<button id="switchButton" type="button" class="button switch">Switch</button>
					</div>
				</form>
			</li>
		</ul>
	</div>		
</div>
<script type="text/javascript">
	jQuery(function() {
		jQuery("#adminOpener").bindAdminDialogInit("/qkview-analyzer");
	});
</script>
			<a href="/qkview-analyzer/logoff" title="Log out of BIG-IP iHealth Viewer">Log out</a>
		</div>
		<div id="center-menu-items">
		</div>
		<div id="right-menu-items">
			<a class="rborder" href="/qkview-analyzer/releaseNotes" title="View BIG-IP iHealth Release Notes" target="new">What's new?</a>
			<a id="feedbackOpener" href="#" class="rborder">Feedback</a>
<div id="feedbackDialog" class="qv-dialog" title="Feedback">
	<div id="feedbackForm">
		<form action="/qkview-analyzer/feedback/new" method="post">
			<p>
				This form is for iHealth or diagnostics related feedback only.  If you are
				encountering issues with F5 equipment in your network, please contact F5&nbsp;Support
				via one of the methods listed 
				<a href="http://support.f5.com/kb/en-us/solutions/public/2000/600/sol2633.html" target="_new"=>here</a>.
			</p> 
			<div>
				<dl>
					<dt>From</dt>
						<dd>a.forster@f5.com</dd>
					<dt>To</dt>
						<dd>BIG-IP iHealth team</dd>
					<dt>Subject</dt>
						<dd>BIG-IP iHealth Viewer application</dd>
				</dl>
				<div class="message-block">
					<label for="feedbackMessage">Message</label>
					<div>
						<input id="qkviewId" name="qkviewId" type="hidden" />
						<textarea id="feedbackMessage" name="feedbackMessage" cols="52" rows="9" class="required" maxlength="2000" style="display:block"></textarea>
					</div>
				</div>
				<div>
					<button id="send" type="button" class="button switch">Send</button>
				</div>
			</div>
			<div class="feedback"></div>
		</form>
	</div>
	<div id="thankYou">
		<div id="thankYouMessage">
			Thank you for providing your feedback.  Your submission has been sent to F5 for evaluation. 
		</div>
		<div>
			<button id="close" type="button" class="button switch">Close</button>
		</div>
	</div>		
</div>
<script type="text/javascript">
	var qvId;
	jQuery(function() {
		jQuery("#feedbackOpener").bindFeedbackDialogInit("#feedbackDialog", qvId);
	});
</script>
			<a href="#" class="rborder">Help</a>
			<div id="help-submenu" class="submenu">
				<ul> 
					<li><a href="http://support.f5.com/kb/en-us/products/big-ip_ltm/manuals/related/bigip_ihealth_user_guide.html" target="_new">User&nbsp;Guide</a></li>
					<li><a href="/qkview-analyzer/about">About</a></li>
				</ul>
			</div>
			<a href="#">F5</a>
			<div id="f5-submenu" class="submenu">
				<ul> 
					<li><a href="http://www.f5.com" target="_new">F5&nbsp;Home</a></li>
					<li><a href="http://support.f5.com/" target="_new">AskF5</a></li>
					<li><a href="http://downloads.f5.com/" target="_new">Downloads</a></li>
				</ul>
			</div>
		</div>
	</div>
	<div id="banner">
		<div id="logo-and-title">
			<div id="logo">
				<a href="/qkview-analyzer/"><img alt="F5 Logo" src="/qkview-analyzer/img/common/logo_f5.png"></a>
			</div>
			<div id="logo-title">
				<h1><a href="/qkview-analyzer/">BIG-IP iHealth</a></h1>
			</div>
		</div>
		<div id="message">
			<div class="close"></div>
			<div class="count"></div>
			<div id="messagetype" class="type request">
				<div class="indicator1"></div><div class="indicator2"></div>
				<div id="messagetitle">Request...</div>
				<div id="messagetext">Receiving data from the iHealth service.</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	jQuery(function() {
		jQuery(".submenu").each(function() {
			jQuery(this).bindSubMenuInit();
		});

		jQuery("#message").each(function() {
			jQuery(this).bindAlertInit("/qkview-analyzer");
		});
	});
</script>
<div id="bd">
	<div class="yui-g home-page">
<div id="maintabs">
    <ul class="ui-tabs-nav">
        <li><a href="#tabs-1">My QKViews</a></li>
        <li><a href="#tabs-2">Saved Comparisons</a></li>
        <li><a href="#tabs-3">Shared QKViews</a></li>
        <li><a href="#tabs-4">Hidden QKViews</a></li>
    </ul>
    <div id="tabs-1"><div id="myQkviews" class="qv-results">
	<div class="fg-toolbar ui-widget-header ui-corner-all ui-helper-clearfix" style="padding: 3px">
		<div class="buttons">
			<button class="upload-link">
				<span class="icon upload">Upload</span>
			</button>
				<button class="diff-link">
					<span class="icon diff">Compare</span>
				</button>
				<button class="hide-link">
					<span class="icon hide">Hide</span>
				</button>
		</div>
		<div class="search-controls">
			<div>
				<input id="showAllCB" type="checkbox" checked="checked" />Search All Users
			</div>
			<div>
				<input id="searchText" name="searchText" type="text" class="search" value="" />
			</div>
			<div>
				<button name="searchButton" id="searchButton">Search</button>
			</div>
		</div>
	</div>
<script type="text/javascript">
	jQuery(function() {
		jQuery('#myQkviews').bindToolbarInit("/qkview-analyzer");
	});
</script>
	<table id="myQkviews-table">
		<thead>
			<tr>
				<th class="hd-toggle">
					<input type="checkbox"/>
				</th>
				<th style="min-width:175px">
					<span>Hostname</span>
				</th>
					<th>
					</th>
				<th style="min-width:85px">
					<span>Version</span>
				</th>
				<th style="min-width:100px">
					<span>Generation Date</span>
				</th>
				<th style="min-width:80px">
					<span>F5 Case</span>
				</th>
				<th style="min-width:125px">
					<span>External Case</span>
				</th>
				<th class="hd-upload-date" style="min-width:75px">
					<span>Upload Date</span>
				</th>
			</tr>
		</thead>
		<tbody>
				<tr>
						<td>
							<input id="myQkviews-800752-cb" type="checkbox"/>
						</td>
						<td>
								<a class="icon open hostname" title="Open QKView: LB72MC25F5.visa.com" href="/qkview-analyzer/qv/800752/status/overview">
LB72MC25F5.visa.com
								</a>
						</td>
						<td>
<span id="myQkviews-800752-commentOpener" 
	class="icon add-new" 
	title="Add comments">
</span>
<div id="commentEditorDialog-myQkviews-800752" class="qv-dialog" title="Comment Editor">
	<div>
		<form action="/qkview-analyzer/qv/800752/comments/edit" class="editor">
	        <p>
				<textarea id="qkviewComments" name="qkviewComments" class="comment-editor" maxlength="2000" ></textarea>
			</p>
	        <div>
	          	<ul>
	          		<li>
	          			<input type="submit" class="submit" value="Cancel" />
	          		</li>
	          		<li>
	          			<input type="submit" class="submit" value="Save" />
	          		</li>
	          	</ul>
	        </div>
	        <div class="feedback"></div>
	    </form>
	</div>
</div>
<script type="text/javascript">
	jQuery(function() {
		jQuery("#myQkviews-800752-commentOpener").bindCommentEditorInit("#commentEditorDialog-myQkviews-800752");
	});
</script>
						</td>
						<td>11.1.0</td>
						<td>Mar 5 2013</td>
					<td id="myQkviews-800752-share">
						<span id="support-case">--</span>
					</td>
					<td>
						<span title="--">
--
						</span>
					</td>
					<td>
Tue, 05 Mar 2013 13:59:15 -0800
					</td>
				</tr>
							<tr>
						<td>
							<input id="myQkviews-800744-cb" type="checkbox"/>
						</td>
						<td>
								<a class="icon open hostname" title="Open QKView: LB72MC24F5.visa.com" href="/qkview-analyzer/qv/800744/status/overview">
LB72MC24F5.visa.com
								</a>
						</td>
						<td>
<span id="myQkviews-800744-commentOpener" 
	class="icon add-new" 
	title="Add comments">
</span>
<div id="commentEditorDialog-myQkviews-800744" class="qv-dialog" title="Comment Editor">
	<div>
		<form action="/qkview-analyzer/qv/800744/comments/edit" class="editor">
	        <p>
				<textarea id="qkviewComments" name="qkviewComments" class="comment-editor" maxlength="2000" ></textarea>
			</p>
	        <div>
	          	<ul>
	          		<li>
	          			<input type="submit" class="submit" value="Cancel" />
	          		</li>
	          		<li>
	          			<input type="submit" class="submit" value="Save" />
	          		</li>
	          	</ul>
	        </div>
	        <div class="feedback"></div>
	    </form>
	</div>
</div>
<script type="text/javascript">
	jQuery(function() {
		jQuery("#myQkviews-800744-commentOpener").bindCommentEditorInit("#commentEditorDialog-myQkviews-800744");
	});
</script>
						</td>
						<td>11.1.0</td>
						<td>Mar 5 2013</td>
					<td id="myQkviews-800744-share">
						<span id="support-case">--</span>
					</td>
					<td>
						<span title="--">
--
						</span>
					</td>
					<td>
Tue, 05 Mar 2013 13:57:12 -0800
					</td>
				</tr>
							<tr>
						<td>
							<input id="myQkviews-770748-cb" type="checkbox"/>
						</td>
						<td>
								<div id="myQkviews-770748"></div>
								<script type="text/javascript">
									jQuery(function() {
										var qvData = {
											id: "770748",
											name: "sjc-lb04.endor.lan (slot 1)",
											host: "sjc-lb04.endor.lan (slot 1)",
											blades: [

{
														id: "770752",
														name: "sjc-lb04.endor.lan (slot 2)",
														host: "sjc-lb04.endor.lan (slot 2)"
												}
												
											]
										}
										jQuery("#myQkviews-770748").bindChassisTreeInit(qvData, "/qkview-analyzer", "/qkview-analyzer/version/2.7.13.8227/wro/application.css");
									});
								</script>
						</td>
						<td>
<span id="myQkviews-770748-commentOpener" 
	class="icon add-new" 
	title="Add comments">
</span>
<div id="commentEditorDialog-myQkviews-770748" class="qv-dialog" title="Comment Editor">
	<div>
		<form action="/qkview-analyzer/qv/770748/comments/edit" class="editor">
	        <p>
				<textarea id="qkviewComments" name="qkviewComments" class="comment-editor" maxlength="2000" ></textarea>
			</p>
	        <div>
	          	<ul>
	          		<li>
	          			<input type="submit" class="submit" value="Cancel" />
	          		</li>
	          		<li>
	          			<input type="submit" class="submit" value="Save" />
	          		</li>
	          	</ul>
	        </div>
	        <div class="feedback"></div>
	    </form>
	</div>
</div>
<script type="text/javascript">
	jQuery(function() {
		jQuery("#myQkviews-770748-commentOpener").bindCommentEditorInit("#commentEditorDialog-myQkviews-770748");
	});
</script>
						</td>
						<td>11.2.1</td>
						<td>Feb 13 2013</td>
					<td id="myQkviews-770748-share">
	<span class="icon locked share-switch" title="Sharing is turned off for this QKView"></span>
<script type="text/javascript">
	jQuery(function() {
		jQuery('#myQkviews-770748-share').find('span.share-switch').bindQvShareToolInit("770748", "/qkview-analyzer");
	});
</script>
						<span id="support-case">1-136242931</span>
					</td>
					<td>
						<span title="--">
--
						</span>
					</td>
					<td>
Wed, 13 Feb 2013 12:07:25 -0800
					</td>
				</tr>
							<tr>
						<td>
							<input id="myQkviews-770680-cb" type="checkbox"/>
						</td>
						<td>
								<div id="myQkviews-770680"></div>
								<script type="text/javascript">
									jQuery(function() {
										var qvData = {
											id: "770680",
											name: "sjc-lb03.endor.lan (slot 2)",
											host: "sjc-lb03.endor.lan (slot 2)",
											blades: [

{
														id: "770684",
														name: "sjc-lb03.endor.lan (slot 1)",
														host: "sjc-lb03.endor.lan (slot 1)"
												}
												
											]
										}
										jQuery("#myQkviews-770680").bindChassisTreeInit(qvData, "/qkview-analyzer", "/qkview-analyzer/version/2.7.13.8227/wro/application.css");
									});
								</script>
						</td>
						<td>
<span id="myQkviews-770680-commentOpener" 
	class="icon add-new" 
	title="Add comments">
</span>
<div id="commentEditorDialog-myQkviews-770680" class="qv-dialog" title="Comment Editor">
	<div>
		<form action="/qkview-analyzer/qv/770680/comments/edit" class="editor">
	        <p>
				<textarea id="qkviewComments" name="qkviewComments" class="comment-editor" maxlength="2000" ></textarea>
			</p>
	        <div>
	          	<ul>
	          		<li>
	          			<input type="submit" class="submit" value="Cancel" />
	          		</li>
	          		<li>
	          			<input type="submit" class="submit" value="Save" />
	          		</li>
	          	</ul>
	        </div>
	        <div class="feedback"></div>
	    </form>
	</div>
</div>
<script type="text/javascript">
	jQuery(function() {
		jQuery("#myQkviews-770680-commentOpener").bindCommentEditorInit("#commentEditorDialog-myQkviews-770680");
	});
</script>
						</td>
						<td>11.2.1</td>
						<td>Feb 13 2013</td>
					<td id="myQkviews-770680-share">
	<span class="icon locked share-switch" title="Sharing is turned off for this QKView"></span>
<script type="text/javascript">
	jQuery(function() {
		jQuery('#myQkviews-770680-share').find('span.share-switch').bindQvShareToolInit("770680", "/qkview-analyzer");
	});
</script>
						<span id="support-case">1-136242931</span>
					</td>
					<td>
						<span title="--">
--
						</span>
					</td>
					<td>
Wed, 13 Feb 2013 11:25:28 -0800
					</td>
				</tr>
			
		</tbody>
	</table>
	<div class="pager-footer"></div>
</div>
<script type="text/javascript">
	jQuery(function() {
		var ids = [];
		var count = 0;
										

		jQuery('#myQkviews').bindDataTableInit("myQkviews");
		if (ids.length > 0) {
			jQuery('#bd').updateProcessingStatus("myQkviews", "/qkview-analyzer", "/qkview-analyzer/version/2.7.13.8227/wro/application.css", ids);
		}
	});
</script></div>
    <div id="tabs-2"></div>
    <div id="tabs-3"></div>
	<div id="tabs-4"></div>
</div>
<script type="text/javascript">
	jQuery(function() {
		jQuery('#maintabs').bindTabsInit("/qkview-analyzer", "0");
	});
	jQuery(document).ready(function() {
		jQuery('#maintabs').css('visibility', 'visible');
	});
</script>
	</div>
</div>
<div id="ft">
	<ul>
		<li><a href="http://www.f5.com/services/customer-support/guidelines-policies/" title="F5 Policies">Policies</a></li>
		<li><a href="/qkview-analyzer/terms/" title="F5's Terms of Use Policy">Terms of Use</a></li>
		<li>&copy; 1998&ndash;2013 F5 Networks, Inc. All rights reserved.</li>
	</ul>
	<span class="appVersion">2.7.13.8227</span>
</div>
		</div>
		<div class="ajax-indicator">
			<img class="ajax-indicator" src="/qkview-analyzer/img/common/spinner.gif" alt="loading..." />
		</div>
<script type="text/javascript">

	var _gaq = _gaq || [];
	_gaq.push(['_setAccount', 'UA-1322557-1']);
	_gaq.push(['_trackPageview']);

  	(function() {
	    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
	    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
	    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  	})();
  
	(function() {
		var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
		ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
		var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	})();

	/*
	 * Click tracking
	 */
	(function() {
		if (typeof(QV) == "undefined") {
			QV = {};
		}
		QV.Analytics = {
			parseQvUrl: function (e) {
				var path = window.location.pathname;
				if (e && e.fragment) {
					var hashtag = e.fragment;
				} else {
					var hashtag = window.location.hash;
				}
				if (hashtag.indexOf("#") == 0) {
					hashtag = hashtag.substring(1, hashtag.length);
				}
				
				var pathArray = path.split("/");
				var url="";
				
				for (var i=0; i< pathArray.length-1; i++) {
					url = url + pathArray[i] + "/";
				}
				url = url + hashtag.replace(":", "/");
				return url;
			}
		};
		
		if (typeof(window._gaq) == "undefined") {
			window._gaq = [];
		}
		
		// Tracking ajax loaded pages
		jQuery(window).bind("hashchange", function(e) {
			var url = QV.Analytics.parseQvUrl(e);
			_gaq.push(['_trackPageview', url]);
		});
		
		jQuery(".status .logout").click(function() {
			_gaq.push(['_trackPageview', '/qkview-analyzer/logout']);
		});
	})();
</script>
	</body>
</html>