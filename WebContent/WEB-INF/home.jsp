<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<title>Portable Diagnostics Tool Results Viewer</title>
		<link rel="shortcut icon" type="image/x-icon" href="/pdtview/assets/images/favicon.ico">
		<link rel="stylesheet" type="text/css" href="/pdtview/assets/pdtview.css">
	</head>
	<body>

<%@include file="includes/header.jsp" %>
	
<!-- Begin My PDT Results (formerly My QKViews) table -->
<!-- This section will definately need to become dynamic and will be a phase 2 thing. Here to make the code work before i figure out how to comment it all out. -->
<!-- This part should be created within the header by whatever fetches the content for the tab you've just clicked on. -->


<%@include file="includes/footer.jsp" %>
	</body>
</html>
