<!--  This is just a placeholder. Here is the resulting HTML table which holds the "My QKViews" list on iHealth. I will want to emulate something similar once I am actually storing things. -->
	<table id="myQkviews-table">
		<thead>
			<tr>
				<th class="hd-toggle">
					<input type="checkbox"/>
				</th>
				<th style="min-width:175px">
					<span>Hostname</span>
				</th>
					<th>
					</th>
				<th style="min-width:85px">
					<span>Version</span>
				</th>
				<th style="min-width:100px">
					<span>Generation Date</span>
				</th>
				<th style="min-width:80px">
					<span>F5 Case</span>
				</th>
				<th style="min-width:125px">
					<span>External Case</span>
				</th>
				<th class="hd-upload-date" style="min-width:75px">
					<span>Upload Date</span>
				</th>
			</tr>
		</thead>
		<tbody>
				<tr>
						<td>
							<input id="myQkviews-800752-cb" type="checkbox"/>
						</td>
						<td>
								<a class="icon open hostname" title="Open QKView: LB72MC25F5.visa.com" href="/qkview-analyzer/qv/800752/status/overview">
LB72MC25F5.visa.com
								</a>
						</td>
						<td>
<span id="myQkviews-800752-commentOpener" 
	class="icon add-new" 
	title="Add comments">
</span>
<div id="commentEditorDialog-myQkviews-800752" class="qv-dialog" title="Comment Editor">
	<div>
		<form action="/qkview-analyzer/qv/800752/comments/edit" class="editor">
	        <p>
				<textarea id="qkviewComments" name="qkviewComments" class="comment-editor" maxlength="2000" ></textarea>
			</p>
	        <div>
	          	<ul>
	          		<li>
	          			<input type="submit" class="submit" value="Cancel" />
	          		</li>
	          		<li>
	          			<input type="submit" class="submit" value="Save" />
	          		</li>
	          	</ul>
	        </div>
	        <div class="feedback"></div>
	    </form>
	</div>
</div>
<script type="text/javascript">
	jQuery(function() {
		jQuery("#myQkviews-800752-commentOpener").bindCommentEditorInit("#commentEditorDialog-myQkviews-800752");
	});
</script>
						</td>
						<td>11.1.0</td>
						<td>Mar 5 2013</td>
					<td id="myQkviews-800752-share">
						<span id="support-case">--</span>
					</td>
					<td>
						<span title="--">
--
						</span>
					</td>
					<td>
Tue, 05 Mar 2013 13:59:15 -0800
					</td>
				</tr>
							<tr>
						<td>
							<input id="myQkviews-800744-cb" type="checkbox"/>
						</td>
						<td>
								<a class="icon open hostname" title="Open QKView: LB72MC24F5.visa.com" href="/qkview-analyzer/qv/800744/status/overview">
LB72MC24F5.visa.com
								</a>
						</td>
						<td>
<span id="myQkviews-800744-commentOpener" 
	class="icon add-new" 
	title="Add comments">
</span>
<div id="commentEditorDialog-myQkviews-800744" class="qv-dialog" title="Comment Editor">
	<div>
		<form action="/qkview-analyzer/qv/800744/comments/edit" class="editor">
	        <p>
				<textarea id="qkviewComments" name="qkviewComments" class="comment-editor" maxlength="2000" ></textarea>
			</p>
	        <div>
	          	<ul>
	          		<li>
	          			<input type="submit" class="submit" value="Cancel" />
	          		</li>
	          		<li>
	          			<input type="submit" class="submit" value="Save" />
	          		</li>
	          	</ul>
	        </div>
	        <div class="feedback"></div>
	    </form>
	</div>
</div>
<script type="text/javascript">
	jQuery(function() {
		jQuery("#myQkviews-800744-commentOpener").bindCommentEditorInit("#commentEditorDialog-myQkviews-800744");
	});
</script>
						</td>
						<td>11.1.0</td>
						<td>Mar 5 2013</td>
					<td id="myQkviews-800744-share">
						<span id="support-case">--</span>
					</td>
					<td>
						<span title="--">
--
						</span>
					</td>
					<td>
Tue, 05 Mar 2013 13:57:12 -0800
					</td>
				</tr>
							<tr>
						<td>
							<input id="myQkviews-770748-cb" type="checkbox"/>
						</td>
						<td>
								<div id="myQkviews-770748"></div>
								<script type="text/javascript">
									jQuery(function() {
										var qvData = {
											id: "770748",
											name: "sjc-lb04.endor.lan (slot 1)",
											host: "sjc-lb04.endor.lan (slot 1)",
											blades: [

{
														id: "770752",
														name: "sjc-lb04.endor.lan (slot 2)",
														host: "sjc-lb04.endor.lan (slot 2)"
												}
												
											]
										}
										jQuery("#myQkviews-770748").bindChassisTreeInit(qvData, "/qkview-analyzer", "/qkview-analyzer/version/2.7.13.8227/wro/application.css");
									});
								</script>
						</td>
						<td>
<span id="myQkviews-770748-commentOpener" 
	class="icon add-new" 
	title="Add comments">
</span>
<div id="commentEditorDialog-myQkviews-770748" class="qv-dialog" title="Comment Editor">
	<div>
		<form action="/qkview-analyzer/qv/770748/comments/edit" class="editor">
	        <p>
				<textarea id="qkviewComments" name="qkviewComments" class="comment-editor" maxlength="2000" ></textarea>
			</p>
	        <div>
	          	<ul>
	          		<li>
	          			<input type="submit" class="submit" value="Cancel" />
	          		</li>
	          		<li>
	          			<input type="submit" class="submit" value="Save" />
	          		</li>
	          	</ul>
	        </div>
	        <div class="feedback"></div>
	    </form>
	</div>
</div> <!-- end commentEditorDialog-myQkviews-770748 -->
<script type="text/javascript">
	jQuery(function() {
		jQuery("#myQkviews-770748-commentOpener").bindCommentEditorInit("#commentEditorDialog-myQkviews-770748");
	});
</script>
						</td>
						<td>11.2.1</td>
						<td>Feb 13 2013</td>
					<td id="myQkviews-770748-share">
	<span class="icon locked share-switch" title="Sharing is turned off for this QKView"></span>
<script type="text/javascript">
	jQuery(function() {
		jQuery('#myQkviews-770748-share').find('span.share-switch').bindQvShareToolInit("770748", "/qkview-analyzer");
	});
</script>
						<span id="support-case">1-136242931</span>
					</td>
					<td>
						<span title="--">
--
						</span>
					</td>
					<td>
Wed, 13 Feb 2013 12:07:25 -0800
					</td>
				</tr>
							<tr>
						<td>
							<input id="myQkviews-770680-cb" type="checkbox"/>
						</td>
						<td>
								<div id="myQkviews-770680"></div>
								<script type="text/javascript">
									jQuery(function() {
										var qvData = {
											id: "770680",
											name: "sjc-lb03.endor.lan (slot 2)",
											host: "sjc-lb03.endor.lan (slot 2)",
											blades: [

{
														id: "770684",
														name: "sjc-lb03.endor.lan (slot 1)",
														host: "sjc-lb03.endor.lan (slot 1)"
												}
												
											]
										}
										jQuery("#myQkviews-770680").bindChassisTreeInit(qvData, "/qkview-analyzer", "/qkview-analyzer/version/2.7.13.8227/wro/application.css");
									});
								</script>
						</td>
						<td>
<span id="myQkviews-770680-commentOpener" 
	class="icon add-new" 
	title="Add comments">
</span>
<div id="commentEditorDialog-myQkviews-770680" class="qv-dialog" title="Comment Editor">
	<div>
		<form action="/qkview-analyzer/qv/770680/comments/edit" class="editor">
	        <p>
				<textarea id="qkviewComments" name="qkviewComments" class="comment-editor" maxlength="2000" ></textarea>
			</p>
	        <div>
	          	<ul>
	          		<li>
	          			<input type="submit" class="submit" value="Cancel" />
	          		</li>
	          		<li>
	          			<input type="submit" class="submit" value="Save" />
	          		</li>
	          	</ul>
	        </div>
	        <div class="feedback"></div>
	    </form>
	</div>
</div>
<script type="text/javascript">
	jQuery(function() {
		jQuery("#myQkviews-770680-commentOpener").bindCommentEditorInit("#commentEditorDialog-myQkviews-770680");
	});
</script>
						</td>
						<td>11.2.1</td>
						<td>Feb 13 2013</td>
					<td id="myQkviews-770680-share">
	<span class="icon locked share-switch" title="Sharing is turned off for this QKView"></span>
<script type="text/javascript">
	jQuery(function() {
		jQuery('#myQkviews-770680-share').find('span.share-switch').bindQvShareToolInit("770680", "/qkview-analyzer");
	});
</script>
						<span id="support-case">1-136242931</span>
					</td>
					<td>
						<span title="--">
--
						</span>
					</td>
					<td>
Wed, 13 Feb 2013 11:25:28 -0800
					</td>
				</tr>
			
		</tbody>
	</table>