<!-- TODO: O.K. long term I need to move this stuff to a .js file that goes in the assents directory instead of an include like this. -->
<!-- TODO: clickedTab is going to turn out to be wrong if I make this clickhandler work for the button as well. -->
	<script>
		//  add event handler for click on tab
		// From http://stackoverflow.com/questions/3550425/pass-a-hidden-field-value-based-on-which-button-is-clicked-with-javascript
		//This worked alert("Javascript is working");
		
		if (typeof String.prototype.startsWith != 'function') {
			// see below for better implementation!
			String.prototype.startsWith = function (str){
				return this.indexOf(str) == 0;
			};
		}
		
		function tab_clickHandler(event) {
		    event = event || window.event; // IE
		    var target = event.target || event.srcElement; // IE
		    var id = target.id;
			//var target = (event.target.id) ? event.target.id : event.srcElement.id;
			//The function is being called. alert("Javascript is working");
			
			if (id.startsWith("mainnavtab")) {
				document.getElementById('activeMainNavTab').value = id;
				//alert("Setting main nav tab to [" + id + "]");
			} else if (id.startsWith("cattab")) {
				document.getElementById('activeCategoryTab').value = id;
				//alert("Setting Cat tab to [" + id + "]");
			} else if (id.startsWith("pritab")) {
				// If it's a primary tab set primary tab
				document.getElementById('activePrimaryTab').value = id;
				//alert("Setting Pri tab to [" + id + "]");
			} else {
				//Otherwise set importance tab
				document.getElementById('activeTab').value = id;
				//alert("Setting active tab to [" + id + "]");
			}
			document.getElementById('VisualizePDTResults').submit();
	    	return false;
		}
		

        // This is how we are expanding or contracting the details section.
        function expandDivsByParent(parentDiv) {
        	//alert("Called for object [" + parentDiv + "]");
           var divs = document.getElementsByTagName('div');
            for (i = 0; i < divs.length; i++) {
            	//Orig if (divs[i].parentNode.id == parentDiv) {
                if (divs[i].parentNode == parentDiv && divs[i].className == "expanded") {
                	if (divs[i].style.display == 'none') {
                		divs[i].style.display = 'block';
        			} else {
        				divs[i].style.display = 'none';
        			}
                }
                //Here I also want to check for class="trigger expand" and then change the image.
            }
        }
		
        // Currently not actually using this method but keeping it around for a little while.
        function expandCollapse(divID) {
        	//This worked so were getting in here. Maybe we just don't have a divID? alert("Javascript is working");
        	
			var vImage = 'commandDivImg';
			if (document.getElementById(divID).style.display == 'none') {
				document.getElementById(divID).style.display = 'block';
			} else {
				document.getElementById(divID).style.display = 'none';
			}
		}
        
        
// 		//I think this is the bit that expands and contracts the details section.
// 		$.fn.bindQvNavigationInit = function (context) {
//        	 $(this).find('div.hd span.icon').each(function () {
//         	    $(this).click(function () {
//             	    var container = $(this).parent().parent().parent();
//                 	if ($(this).hasClass('expand')) {
//      	               $(this).removeClass('expand').addClass('collapse');
//         	            $(container).find('div.bd').show();
//             	    } else {
//                 	    $(this).removeClass('collapse').addClass('expand');
//                     	$(container).find('div.bd').hide();
//     	            }
//         	        $(container).siblings().each(function () {
//             	        var navLink = $(this).find('div.hd span.icon');
//                 	    if ($(navLink).hasClass('collapse')) {
//                     	    $(navLink).removeClass('collapse').addClass('expand');
//   	                      $(this).find('div.bd').hide();
//     	                }
//         	        });
//             	});
//   	      });
//           $(this).find('div.hd span.collapse').each(function () {
//             	$(this).parent().siblings('div.bd').show();
//     	   });
//     	}
		
// 		//or this
// 		$("#contentBlock .trigger-section .show").click(function (e) {
//             e.preventDefault();
//             $("#contentBlock .trigger").removeClass("expand").addClass("collapse");
//             if (isSearch) {
//                 $("#contentBlock .trigger").parent().parent().siblings(".expanded").show();
//             } else {
//                 $("#contentBlock .trigger").siblings(".expanded").show();
//             }
//         });
//         $("#contentBlock .trigger-section .hide").click(function (e) {
//             e.preventDefault();
//             $("#contentBlock .trigger").removeClass("collapse").addClass("expand");
//             if (isSearch) {
//                 $("#contentBlock .trigger").parent().parent().siblings(".expanded").hide();
//             } else {
//                 $("#contentBlock .trigger").siblings(".expanded").hide();
//             }
//         });
        
	</script>