<!-- This is going to be the included code that displays the xml data that was submitted. -->
<!-- <form name="VisualizePDTResults" method="post" action="home.jsp"> -->
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td>
				Visualized Results
			</td>
		</tr>
		<tr>
			<td>
			<!-- I'm not sure why we need the check for null there but there it is -->
			<span class="error">${messages.PDTResultsXML}</span>
	    	<span class="success">${messages.success}</span>
			<%
	    		if(request.getParameter("PDTResultsXML") != null){
	    				if(request.getParameter("PDTResultsXML").equals("")) {
	    					out.println("<p>Sorry, you need to enter some actual XML. Please try again</p>");
	    				} else {
	    					out.println("<p>The form was submitted. Let's analyse the data</p>");
		 					/*out.println("<p>You submitted the form with [" + request.getParameter("PDTResultsXML") + "]");*/
	    				}
	    		} else {
	    			out.println("<p>Ooops, the PDTResultsXML parameter is null</p>");
	    		}
	    	%>


	    	<!--  TODO: I need to add a way for them to submit a new one. Do I want a button that reloads via get? -->
			</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
		</tr>
	</table>
<!-- </form>-->
	</body>
</html>