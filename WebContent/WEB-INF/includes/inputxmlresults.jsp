<!-- This is going to generate the form portion of the page where you can enter your xml and hit submit to visualize it. -->
<!-- Is it bad to be redundent and include this here? -->
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!-- TODO: Figure out how to make this overview section hidden and show it once they've submitted content. -->
    	<div id="qv-content">
    	<!-- TODO: This is where I should have something separate loaded depending on the tab we're loading. -->
    	
	    	<h2 class="title">Diagnostics</h2>
    			<div id="contentBlock">
    				<!-- <div class="tools">
    				Keeping the tools bit here because I believe I will be sticking something else in here.
    				</div> --> <!-- close tools -->
						<div id="heuristics" class="main yui-t1" style="display: block;">

	<div id="overview" class="section">
		<div class="hd">
			<h3>System Overview</h3>
		</div>
		<div class="bd" style="height: 70px;">
			<ul class="data">
				<li>
					<dl>
						<dt>Hostname</dt>
						<dd>${messages.sysinfo_hostname}</dd>
					</dl>
				</li>
				<li class="even">
				<!-- TODO: For viprions we get a chassis SN and a blade SN instead of just a device SN.
				A) Figure out if we're a viprion and display both and B) See what it looks like if it's not a viprion the SN variable might not be the same as the one I'm using. -->
					<dl>
						<dt>Chassis S/N</dt>
						<dd>${messages.sysinfo_bigip_chassis_serial_num}</dd>
					</dl>
				</li>
				<li>
					<dl>
						<dt>System Version</dt>
						<dd>${messages.sysinfo_Product}  ${messages.sysinfo_Version}  (Build ${messages.sysinfo_Built}  ${messages.sysinfo_Edition})</dd>
					</dl>
				</li>
					<li class="even">
					<dl>
						<dt>Platform</dt>
						<dd>${messages.sysinfo_platform}</dd>
					</dl>
				</li>
			</ul> <!-- close data list --> 
		</div> <!-- close bd -->
	</div> <!-- close overview -->

<div id="primary-tabs" class="ui-tabs ui-widget ui-widget-content ui-corner-all">
								<ul class="ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all">
								<!-- <li class="low ui-state-default ui-corner-top <c:out value="${uidirectives.lowlishow}"></c:out>">-->
									<!-- Original <li class="ui-state-default ui-corner-top ui-tabs-selected ui-state-active" title="Diagnostic checks that found potential issues on this unit"> -->
									<li class="ui-state-default ui-corner-top <c:out value="${uidirectives.pritruelishow}"></c:out>" title="Diagnostic checks that found potential issues on this unit">
										<a id="pritab-true" href="javascript:void(0)" onclick="tab_clickHandler(event)">
											Identified (
												<span><c:out value="${messages.numFailed}">0</c:out></span>
											)
										</a>
									</li>
									<li class="ui-state-default ui-corner-top <c:out value="${uidirectives.prifalselishow}"></c:out>" title="Additional checks that did not find issues on this unit">
										<a id="pritab-false" href="javascript:void(0)" onclick="tab_clickHandler(event)">
											Passed (
												<span><c:out value="${messages.numPassed}">0</c:out></span>
											)
										</a>
									</li>
								</ul>
							<!-- Original 							<div id="diagnostics:identified" class="ui-tabs-panel ui-widget-content ui-corner-bottom"> -->
							<div id="diagnostics:identified" class="ui-tabs-panel ui-widget-content">
								<div id="identified-tabs" class="ui-tabs ui-widget ui-widget-content ui-corner-all" >
								<!-- this didn't quite work out. <ul class="tabset ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all" style="background-color: #E6E6E6; background:#ccc url(/pdtview/assets/images/ui-bg_highlight-soft_75_cccccc_1x100.png) 50% 50% repeat-x"> -->
									<ul class="tabset ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all" style="background-color: #E6E6E6;">
										<li class="critical ui-state-default ui-corner-top <c:out value="${uidirectives.criticallishow}"></c:out>">
											<!-- <a href="#diagnostics:identified:critical" onclick="tab_clickHandler(this)"> -->
											<!-- TODO: Change this from directly calling onclick within each and every damned link to using the CSS. there's a great example here http://stackoverflow.com/questions/134845/href-attribute-for-javascript-links-or-javascriptvoid0 -->
											<a id="tab-critical" href="javascript:void(0)" onclick="tab_clickHandler(event)">
												Critical (
													<span><c:out value="${messages.numCrit}">0</c:out></span>
												)
											</a>
										</li>
										<li class="high ui-state-default ui-corner-top <c:out value="${uidirectives.highlishow}"></c:out>">
											<a id="tab-high" href="javascript:void(0)" onclick="tab_clickHandler(event)">
												High (
													<span><c:out value="${messages.numHigh}">0</c:out></span>
												)
											</a>
										</li>
										<li class="medium ui-state-default ui-corner-top <c:out value="${uidirectives.mediumlishow}"></c:out>">
											<a id="tab-medium" href="javascript:void(0)" onclick="tab_clickHandler(event)">
												Medium (
													<span><c:out value="${messages.numMed}">0</c:out></span>
												)
											</a>
										</li>
										<li class="low ui-state-default ui-corner-top <c:out value="${uidirectives.lowlishow}"></c:out>">
											<a id="tab-low" href="javascript:void(0)" onclick="tab_clickHandler(event)">
												Low (
													<span><c:out value="${messages.numLow}">0</c:out></span>
												)
											</a>
										</li>
									</ul>
									<!-- Original class content <div id="diagnostics:identified:critical" class="critical ui-tabs-panel ui-widget-content ui-corner-bottom ui-tabs-hide"> -->
									<div id="diagnostics:identified:critical" class="critical ui-tabs-panel ui-widget-content <c:out value="${uidirectives.criticaltabshow}"></c:out>">
									</div>
									<div id="diagnostics:identified:high" class="high ui-tabs-panel ui-widget-content <c:out value="${uidirectives.hightabshow}">ui-tabs-hide</c:out>">
									</div>
									<div id="diagnostics:identified:medium" class="medium ui-tabs-panel ui-widget-content <c:out value="${uidirectives.mediumtabshow}">ui-tabs-hide</c:out>">
									</div>
									<div id="diagnostics:identified:low" class="low ui-tabs-panel ui-widget-content <c:out value="${uidirectives.lowtabshow}">ui-tabs-hide</c:out>">
									</div>
									<div id="diagnostics:current" class="<c:out value="${uidirectives.showResults}">high</c:out>">
										<!-- TODO: Modify this to use my correct vars and get rid of the table and use the proper divs. -->
										<c:if test="${not empty listTests}">
											<!-- <p>List is of size:<c:out value="${fn:length(listTests)}"/></p> -->
     										<c:forEach var="ob" varStatus="status" items="${listTests}">
										<div class="result">
											<div class="summary">
												<h3 class="title">
													<c:out value="${ob.header}"/>
												</h3>
												<div class="upgrade">
												<!-- 	<h4>Recommended upgrade version</h4>
													<ul>
														<li>None</li>
													</ul>  -->
												</div>
												<div class="solutions">
													<!-- TODO: See if there's a way to properly build solutions linke so I can put this back to <h4>Solution Links</h4> -->
													<h4>Solutions</h4>
													<c:if test="${not empty ob.solutions}">
														<c:forEach var="sol" varStatus="status" items="${ob.solutions}">
															<ul>
																<li>${sol}</li>
															</ul>
														</c:forEach>
            										</c:if>
												</div>
												<div class="solutions">
													<h4>Heuristic Name</h4>
													<ul>
														<li><c:out value="${ob.name}"/></li>
													</ul>
												</div>
												<div class="ft"></div>
											</div> <!-- close summary -->
											<!-- Figure out how to make this bit work. -->
											<!-- orig <div id="button<c:out value="${ob.name}"/>" class="trigger expand" title="Show/Hide" onclick="expandDivsByParent(this.id)"> -->
											<div id="button<c:out value="${ob.name}"/>" class="trigger expand" title="Show/Hide" onclick="expandDivsByParent(this.parentNode)">
												<span class="label">Details</span>
											</div> <!-- close trigger expanded -->
												<div class="expanded" style="display: none;">
													<div class="crs">
														<!-- <h4>Related Changes</h4>
														<ul>
															<li>Not Implemented</li>
														</ul> -->
													</div>
													<div class="desc">
														<h4>Description</h4>
														<p><c:out value="${ob.summary}"/></p>
													</div>
													<div class="resolution"> <!-- Read action -->
														<h4>Recommendation resolution</h4>
														<p><c:out value="${ob.action}"/></p>
													</div>
													<div class="details">
													<!-- 	<h4>Additional Information</h4>
															<p>None</p>-->
													</div>
												</div> <!-- close expanded -->
												<div class="ft"></div>
											
											</div> <!-- close result -->


<!--     											<tr>
        											<td>name is:<c:out value="${ob.name}"/></td>
        											<td>importance is:<c:out value="${ob.importance}"/></td>
        											<td>Matched is:<c:out value="${ob.matched}"/></td>
        											<td>Action is:<c:out value="${ob.action}"/></td>
        											<td>Header is:<c:out value="${ob.header}"/></td>
        											<td>Summary is:<c:out value="${ob.summary}"/></td>
    											</tr> -->

             								</c:forEach>
            							</c:if>
									</div> <!-- close current results diagnostics identified -->



									</div> <!-- close identified-tabs -->
									
									
								</div> <!-- close diagnostics-identified -->
							</div> <!-- close primary-tabs -->

<!-- TODO: Right now this is outside of the tabs div structure. move it inside? Figure out how to move it inside and still only have one tab contain content/display at a time. -->


	<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<!-- TODO: Wrap this in an hd/bd section like the above and hide it unless there's an actual message -->
		<tr>
			<td>
				<span class="error">${messages.PDTResultsXML}</span>
				<span class="success">${messages.success}</span>
			</td>
		</tr>
		<tr>
			<td>
				Enter PDT XML Results
			</td>
		</tr>
		<tr>
			<td>
				<input type="submit" name="submit_button" value="Submit">
			</td>
		</tr>
		<tr>
			<td>
				<!-- TODO: In a perfect world I'll find the javascript I need so that I can have some sample text in there and blank it out when you click or tab into the field. -->
				<textarea rows="30" cols="50" name="PDTResultsXML" style="width: 100%; height: 100%;">${fn:escapeXml(param.PDTResultsXML)}</textarea>
				<!--<p>
                <label for="name">What's your name?</label>
                <input id="name" name="name" value="${fn:escapeXml(param.name)}">
            	</p>-->
			</td>
		</tr>
		<tr>
			<td>
				<input type="submit" name="submit_button" value="Submit">
			</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
		</tr>
	</table>
							<div id="diagnostics:passed" class="ui-tabs-panel ui-widget-content ui-corner-bottom ui-tabs-hide"></div> <!-- close diagnostics:passed -->
							
													</div> <!-- close heuristics -->
				</div> <!-- close contentBlock -->
			</div> <!-- close qv-content -->
