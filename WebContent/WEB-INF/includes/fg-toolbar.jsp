<!-- This is just here to save it. fg-toolbar is the bit that generates the persistent toolbar which sits _below_ the navigation tabs and contains buttons for uploading a new qkview, comparing multiple qkviews and a search box. -->
	<div class="fg-toolbar ui-widget-header ui-corner-all ui-helper-clearfix" style="padding: 3px" id="buttonsAndControls">
		<div class="buttons">
			<button class="upload-link">
				<span class="icon upload">Upload</span>
			</button>
				<button class="diff-link">
					<span class="icon diff">Compare</span>
				</button>
				<button class="hide-link">
					<span class="icon hide">Hide</span>
				</button>
		</div> <!-- end buttons -->
		<div class="search-controls">
			<div>
				<input id="showAllCB" type="checkbox" checked="checked" />Search All Users
			</div>
			<div>
				<input id="searchText" name="searchText" type="text" class="search" value="" />
			</div>
			<div>
				<button name="searchButton" id="searchButton">Search</button>
			</div>
		</div> <!-- end search-controls -->
	</div> <!-- end buttonsAndControls -->