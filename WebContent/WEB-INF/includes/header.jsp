<!-- TODO: This should be the code that creates the top bar and then the navigation bar. The navigation bar code should load the active tab so there should likely be another reference to the bit that loads the tab code. That _should_ turn into something that a servlet hands me but for now it will be JSP -->
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
			<div class="yui-d3f" id="mainPage">
			<%@include file="../includes/JavaScript.jsp" %>
<!-- Some useful HTTP encoded characters

&#91; == [ open square brace
&#93; == ] close square brace

&#58; == : colon
&#59; == ; semicolon

&#123; == { open curly brace
&#125; == } close curly brace

&#60; or &lt; == < less than
&#62; or &gt; == > greater than

From http://ascii.cl/htmlcodes.htm
 -->
 
<!-- Within the header include there's also center left and right menu items that are probably appropriate for their own includes. -->
<div id="header">
	<div id="banner">
		<div id="logo-and-title">
			<div id="logo">
				<a href="/pdtview/"><img alt="F5 Logo" src="/pdtview/assets/images/logo_f5.png"></a>
			</div>
			<div id="logo-title">
				<h1><a href="/pdtview/">Portable Diagnostics Tool Viewer</a></h1>
			</div>
		</div>
		<!-- TODO: Make it so that this only shows if we do actually need a status message -->
		<!-- TODO: Then make it a servlet request or something that takes a parameterized message title and text. -->

	</div>  <!-- close banner -->
</div> <!-- close Header -->



<!-- Begin Main Body -->
<!-- I think 'bd' is short for 'body' which is stupid, be less obscure -->
<div id="bd">
	<div class="yui-g home-page">
<form id="VisualizePDTResults" name="VisualizePDTResults" method="post" action="home">
		<!-- This is a wierd little hack that I'm doing so that the choose statement below will understand what page it is supposed to load -->
		<c:set var="anotherActiveMainNavTabVar" scope="session" value="activeMainNavTab.name"/><!-- Not currently in use. I doubt this will work yet but I'm trying to get the value of the below form element so I can c:choose based on that. -->
		<!-- Set which tabs should be active on the next click event. Use default values if not provided (IE on first load) -->
		<input type="hidden" id="activeMainNavTab" name="activeMainNavTab" value="<c:out value="${messages.activeMainNavTab}">mainnavtab-quickvisualize</c:out>">
		<input type="hidden" id="activePrimaryTab" name="activePrimaryTab" value="<c:out value="${messages.activePrimaryTab}">pritab-true</c:out>">
		<input type="hidden" id="activeTab" name="activeTab" value="<c:out value="${messages.activeTab}">crit</c:out>">

	<!-- Begin Navigation Header -->
<div id="maintabs"  class="ui-tabs ui-widget ui-widget-content ui-corner-all" style="visibility: visible;">
    <ul class="ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all">
    	<!-- The first page will just be a text box where you paste in the xml and it presents it in a navicable format. -->
    	<!-- TODO: Put the ui-tabs-selected and ui-state-active items on the 'Visualize a PDT output file' link insice of a c:out default so I can get rid of them later without having to resort to JS. -->
    	<li class="critical ui-state-default ui-corner-top <c:out value="${uidirectives.mainnavquickvisualizelishow}">ui-tabs-selected ui-state-active</c:out>">
    		<a id="mainnavtab-quickvisualize" href="javascript:void(0)" onclick="tab_clickHandler(event)">
    			Visualize a PDT output file
    		</a>
    	</li>
    	
    	<!-- <li class="ui-state-default ui-corner-top ui-tabs-selected ui-state-active"><a href="javascript:void(0)">Visualize a PDT output file</a></li> -->
    	
     	<li class="critical ui-state-default ui-corner-top <c:out value="${uidirectives.mainnavmyqkviewslishow}"></c:out>">
    		<a id="mainnavtab-myqkviews" href="javascript:void(0)" onclick="tab_clickHandler(event)">
    			My QKViews
    		</a>
    	</li>
    	<li class="critical ui-state-default ui-corner-top <c:out value="${uidirectives.mainnavuploadqkviewlishow}"></c:out>">
    		<a id="mainnavtab-uploadqkview" href="javascript:void(0)" onclick="tab_clickHandler(event)">
    			Upload a QKView
    		</a>
    	</li>
    	<!-- TODO: Decide if 'upload a qkview' will remain a separate tab or if it will be more like the way that iHealth works where it's like it's own little entity. I'm not terribly fond of that. -->
    	<!-- Later you will be able to upload the files and store them to have historical and potentially comparisons -->
    	
    	<!-- Eventually I'd like to be able to have users upload a QKView and then we will submit it to PDTool and get the results back and give you navicable functionality like iHealth -->
        <!-- <li class="ui-state-default ui-corner-top"><a href="#tabs-2">My PDT Output files</a></li> -->
        <!-- TODO: See if I can do something like this -->
        <!-- <li class="ui-state-default ui-corner-top"><a href="#tabs-3">Saved Comparisons</a></li> -->
    </ul>
    <div id="tabs-1" class="ui-tabs-panel ui-widget-content ui-corner-bottom li.ui-tabs-selected">
    	<div id="myPDTResults" class="qv-results">
<!-- Begin This is where the main body is being modified -->
    	
    	<!--  I need to figure out how to set this css when you click on a tab. #maintabs .ui-tabs-nav li.ui-tabs-selected -->
    	
    	<div id="qv">
		<div class="breadcrumbs">
		<!-- TODO: Put some breadcrumbs in here -->
		</div> <!-- close breadcrumbs -->
		<div id="qvhd">
		</div> <!-- close qvhd -->		
    	<div class="yui-t2">
			<!-- <div id="qvnav" class="yui-b">
				<p>navstuffhere</p>
			</div> --> <!-- close qvnav -->
			<!-- <div class="yui-main"> -->
		<div id="qvmain" class="yui-t2">
<!-- 	<c:if test="${messages.activeMainNavTab == mainnavtab-quickvisualize}"> 
			
		</c:if>  -->
		<p>messages  activeMainNavTab contains &#91;<c:out value="${messages.activeMainNavTab}">Nothing</c:out>&#93;</p>
    	<c:choose>
    		<c:when test="${messages.activeMainNavTab eq 'mainnavtab-myqkviews'}">
    			<p>NOT IMPLEMENTED</p>
    		</c:when>
    		<c:when test="${messages.activeMainNavTab eq 'mainnavtab-uploadqkview'}">
    			<%@include file="../includes/UploadQKView.jsp"%>
    		</c:when>
    		<c:otherwise>
    			<!-- activeMainNavTab == mainnavtab-quickvisualize -->
    			<%@include file="../includes/inputxmlresults.jsp"%>
    		</c:otherwise>
    	</c:choose>
						
    							
    							
			
				</div> <!-- close qvmain -->
			<!-- </div> --> <!-- close class = yui-main -->
		</div> <!-- close class=yui-t2 -->
	</div> <!-- close qv -->

<!-- End This is where the main body is being modified -->


<!-- End Navigation Header (sort of) This div isn't actually closed yet in the regular code the tabs change based on javaScript so that might change.-->
	<div class="pager-footer"></div>
</div> <!-- end myPDTResults -->
<!-- End My PDT Results (formerly My QKViewstable -->




</div> <!-- end tabs-1 -->
<!-- Ok, I think this is part of the JS. the other tabs just have the div there and probably beomc real once you click them. I will change it to Javaz -->
    <div id="tabs-2"></div>
    <div id="tabs-3"></div>
	<!-- <div id="tabs-4"></div> -->
</div> <!-- end maintabs -->
	</div> <!-- end divyui-g home-page -->
</div> <!-- end bd -->