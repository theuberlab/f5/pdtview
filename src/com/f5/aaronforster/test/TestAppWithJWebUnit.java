/**
 * 
 */
package com.f5.aaronforster.test;

import static net.sourceforge.jwebunit.junit.JWebUnit.assertTitleEquals;
import static net.sourceforge.jwebunit.junit.JWebUnit.beginAt;
import static net.sourceforge.jwebunit.junit.JWebUnit.setBaseUrl;
import static net.sourceforge.jwebunit.junit.JWebUnit.setTextField;
import static net.sourceforge.jwebunit.junit.JWebUnit.submit;
import static net.sourceforge.jwebunit.junit.JWebUnit.clickLink;
import static net.sourceforge.jwebunit.junit.JWebUnit.assertLinkPresentWithText;

import java.io.FileInputStream;
import java.io.IOException;

import org.apache.commons.io.IOUtils;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.f5.aaronforster.pdtview.util.PDTViewExceptionHandler;


/**
 * @author forster
 *
 */
public class TestAppWithJWebUnit {
//	protected File fXmlFile = new File("SamplePDToolOutput.xml");
	protected FileInputStream inputStream = null;
	protected String xmlResults = null;
//	protected FileInputStream inputStream = null;
	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}
	
	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}
	
	/**
	 * @throws java.lang.Exception
	 */
    @Before
    public void prepare() {
        setBaseUrl("http://localhost:9608/pdtview/");
    }
    
	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}
	
	public TestAppWithJWebUnit() {
		try {
			inputStream = new FileInputStream("SamplePDToolOutput.xml");
			xmlResults = IOUtils.toString(inputStream);
		} catch ( IOException e) {
			PDTViewExceptionHandler exceptionHandler = new PDTViewExceptionHandler(e);
			exceptionHandler.processException();
		} finally {
	        try {
				inputStream.close();
			} catch (IOException e) {
				PDTViewExceptionHandler exceptionHandler = new PDTViewExceptionHandler(e);
				exceptionHandler.processException();
			}
	    }		

//	    BufferedReader br = new BufferedReader(new FileReader("file.txt"));
//	    try {
//	        StringBuilder sb = new StringBuilder();
//	        String line = br.readLine();
//
//	        while (line != null) {
//	            sb.append(line);
//	            sb.append("\n");
//	            line = br.readLine();
//	        }
//	        String everything = sb.toString();
//	    } finally {
//	        br.close();
//	    }

	
	}
	
	
		
    @Test
    public void test1() {
        beginAt("/"); //Open the browser on http://localhost:9608/pdtview/
        assertTitleEquals("Portable Diagnostics Tool Results Viewer");
        setTextField("PDTResultsXML", xmlResults);
        submit();
        assertLinkPresentWithText("Identified ( 9 )");
        assertLinkPresentWithText("Passed ( 121 )");
        assertLinkPresentWithText("Critical ( 0 )");
        assertLinkPresentWithText("High ( 2 )");
        assertLinkPresentWithText("Medium ( 2 )");
        assertLinkPresentWithText("Low ( 5 )");
        
        clickLink("pritab-false");
        assertLinkPresentWithText("Critical ( 15 )");
        assertLinkPresentWithText("High ( 18 )");
        assertLinkPresentWithText("Medium ( 53 )");
        assertLinkPresentWithText("Low ( 5 )");
        clickLink("tab-high");
        
        
        

        
        
//        Identified ( 9 )
//        Passed ( 121 )
//
//        Critical ( 0 )
//        High ( 2 )
//        Medium ( 2 )
//        Low ( 5 )

//        assertTitleEquals("Welcome, test!");
    }
    
    
//    Now I need to get the contents of the span with in the a href with id 'pritab-true' and make sure it's 9
    
    
    
}
