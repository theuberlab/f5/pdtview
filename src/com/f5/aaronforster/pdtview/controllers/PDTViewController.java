package com.f5.aaronforster.pdtview.controllers;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.xpath.XPathConstants;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.DOMException;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.f5.aaronforster.pdtview.objects.HeuristicResult;
import com.f5.aaronforster.pdtview.objects.Results;
import com.f5.aaronforster.pdtview.util.PDTViewExceptionHandler;
import com.f5.aaronforster.pdtview.util.XPathReader;
//import java.io.File;
//import javax.servlet.annotation.WebServlet;

// ##################################################################################################################
// ###                                       THE GLOBAL TODO SECTION                                              ###
// ##################################################################################################################
// Global TODOs have been moved into bugzilla. I still need to go through the body and find in-line TODOS and move those.

// ##################################################################################################################
// ###                                           Version History                                                  ###
// ##################################################################################################################
// 1.0: A single page. Simply takes the xml output of pdtool and carves it up so that it can be navigated the way that 
//		the results can be navigated on iHealth.
//
// 2.0: Currently in development. 2.0 will allow you to upload a qkview and will submit it to pdtool itself. 
// 		It will also store the qkviews and their analysis for future retrieval/analisys.
//
// Future: Future versions will include the ability to view files like iHealth and the ability to compare multiple 
//		QKviews to see what has changed in the interim.
//


//@WebServlet("/PDTViewController")
/**
 * PDTViewController.
 * 
 * This is the bit of code that will actually be processing the XML and stuff.
 * 
 * @author Aaron Forster @date 20130401
 * @version 1.1
 */
public class PDTViewController extends HttpServlet {
	/**
	 * A handle to the slf4j logging subsystem.
	 */
	private Logger log = LoggerFactory.getLogger(PDTViewController.class);
	/**
	 * The IP Address of the Portable Diagnostics Tool web interface.
	 */
	private InetAddress pdtHost;
	/**
	 * The port of the Portable Diagnostics Tool web interface.
	 */
	private String pdtPort;
	/**
	 * Serial version ID. Mostly to get rid of the IDE complaining about it but also because I will probably start serializing in the future.
	 */
	static final long serialVersionUID = 248657139684L;
	/**
	 * holds the current JSP we are submitting to. Right now it's all home.jsp but I think some things will move into separate JSP pages instead of many, many includes.
	 */
	private String activeJSPPage = "/WEB-INF/home.jsp";
	
	/**
	 * Default constructor.
	 */
	public PDTViewController() {
		try {
			pdtHost = InetAddress.getByName("192.168.230.101");
			pdtPort = "8080"; //TODO: Confirm this.
		} catch (UnknownHostException e) {
			PDTViewExceptionHandler exceptionHandler = new PDTViewExceptionHandler(e);
			exceptionHandler.processException();
		}

	}
	

	/**
	 * Called when requested via GET method. Simply forwards the request to the main jsp file
	 * 
	 */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // Preprocess request: we actually don't need to do any business stuff, so just display JSP.
        request.getRequestDispatcher("/WEB-INF/home.jsp").forward(request, response);
    }
    
    /**
	 * Called when requested via POST method. It's going to make sure that the XML is valid and then pass it to the processor.
	 * 
	 */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // Postprocess request: gather and validate submitted data and display result in same JSP.
    	/**
    	 * The names of the main navigation tabs. Used to iterate later.
    	 */
    	List<String> MainNavTabs = new ArrayList<String>(Arrays.asList("quickvisualize", "myqkviews", "uploadqkview"));
    	/**
    	 * The passed/identified status tabs.
    	 */
    	List<String> primaryTabs = new ArrayList<String>(Arrays.asList("true", "false"));
    	/**
    	 * Test importance categories. IE Critical, High, Medium and Low.
    	 */
    	List<String> importanceLevels = new ArrayList<String>(Arrays.asList("critical", "high", "medium", "low"));
    	
    	// Get the selected tabs from the gui
    	/**
    	 * The currently active Main tab.
    	 */
    	String activeMainNavTab = request.getParameter("activeMainNavTab");
    	/**
    	 * The currently active passed/identified tab.
    	 */
    	String activePrimaryTab = request.getParameter("activePrimaryTab");
    	/**
    	 * The currently active importance tab.
    	 */
    	String activeTab = request.getParameter("activeTab");
    	log.debug("Active Tabs will be Navigation [{}] Primary [{}] Secondary [{}]", activeMainNavTab, activePrimaryTab, activeTab);
    	
    	//TODO: Move these inside of Results
    	/**
    	 * A place to store test results. Needs to be moved inside of the Results object.
    	 */
    	List<HeuristicResult> tests = new ArrayList<HeuristicResult>();
    	
        /**
         * Stores messages which will be sent back to the user. This includes the output from tests/analysis.
         */
        Map<String, String> messages = new HashMap<String, String>();
        request.setAttribute("messages", messages);
        
        /**
         * Stores settings which will control the appearance of items in the GUI.
         */
        Map<String, String> uidirectives = new HashMap<String, String>();
        request.setAttribute("uidirectives", uidirectives);
        
        
        /**
         * Stores the XML Results output from pdtool. Can be entered by a user via the Quick Visualize page or returned from pdtool in response to submitting a qkview to it.
         */
        String PDTResultsXML = request.getParameter("PDTResultsXML");
        if (PDTResultsXML == null || PDTResultsXML.trim().isEmpty()) {
            messages.put("PDTResultsXML", "Please enter some XML");

        }
        //TODO: Replace this with an xml validator
//    	} else if (!PDTResultsXML.matches("\\p{Alnum}+")) {
//            messages.put("PDTResultsXML", "Please enter alphanumeric characters only. Of course xml isn't just alnum but this is a test.");
//        }

        // No validation errors? Do the business job!
        if (messages.isEmpty()) {
        	Results myResults = new Results();
        	
        	try {
        		XPathReader reader = new XPathReader(PDTResultsXML);    			
    			
    			//optional, but recommended
    			//read this - http://stackoverflow.com/questions/13786607/normalization-in-dom-parsing-with-java-how-does-it-work
    			//TODO: Figure out how to do this with xpath.
//    			doc.getDocumentElement().normalize();
    			
            	// First get the overview; Host name, serial number, version.        	        
       	        List<String> arrayList = new ArrayList<String>(Arrays.asList("hostname", "platform", "bigip_chassis_serial_num"));
       	        for (String s : arrayList) {
       	        	String thisResult = (String) reader.read("/diagnostic_output/system_information/" + s, XPathConstants.STRING);
           	        log.debug("Result for [{}] is ", s, thisResult);
           	        messages.put("sysinfo_" + s, thisResult);
       	        }
       	        
       	        arrayList = new ArrayList<String>(Arrays.asList("Version", "full_dot_notation", "Product", "Built", "Edition"));
       	        for (String s : arrayList) {
       	        	String thisResult = (String) reader.read("/diagnostic_output/version/" + s, XPathConstants.STRING);
       	        	log.debug("Result for [{}] is ", s, thisResult);
           	        messages.put("sysinfo_" + s, thisResult);
       	        }
       	        
    			
//    			NodeList nList = doc.getElementsByTagName("staff");
        
       	        log.info("----------------------------");
    			
    	        //A great example of using xpath and dom with xml.
    	        //http://stackoverflow.com/questions/11649396/java-dom-parser-reports-wrong-number-of-child-nodes
    			
    	        NodeList nList = (NodeList) reader.read("/diagnostic_output/diagnostic[*]", XPathConstants.NODESET);
//    	        Element hostname = (Element) reader.read("/diagnostic_output/system_information/hostname", XPathConstants.NODE);
    	        
    	        log.debug("NodeList nList length is [{}]", nList.getLength());
    	        
				//TODO: I should check whatever default importance level I'm using and go down to the next one if it's got zero results.
				boolean selectedResultStatus = Boolean.parseBoolean(activePrimaryTab.replace("pritab-", ""));
				String selectedImportance = activeTab.replace("tab-", "");
				String selectedMainNavCategory = activeMainNavTab.replace("mainnavtab-", "");
				
				
    			for (int i = 0; i < nList.getLength(); i++) {
    				Node nNode = nList.item(i);
    				NamedNodeMap attributes = nNode.getAttributes();
    				String myName = attributes.getNamedItem("name").getTextContent().toString();
    				
    				log.debug("Test name [{}]", myName);
    				
    				if (nNode.getNodeType() == Node.ELEMENT_NODE) {
    					Element eElement = (Element) nNode;
    					
    					//TODO: Move some of this stuff like summary and sol selection after we decide if we care.
    					String importance = eElement.getElementsByTagName("h_importance").item(0).getTextContent();
    					log.debug("Matched contains [{}]", eElement.getElementsByTagName("match").item(0).getTextContent());
    					boolean matched = Boolean.parseBoolean(eElement.getElementsByTagName("match").item(0).getTextContent());
    					String action = eElement.getElementsByTagName("h_action").item(0).getTextContent(); 
    					String header = eElement.getElementsByTagName("h_header").item(0).getTextContent(); 
    					String summary = eElement.getElementsByTagName("h_summary").item(0).getTextContent();
    					
    			    	// A place to store teh sols.
    			    	List<String> solsList = new ArrayList<String>();
    			    	Element sols = (Element) eElement.getElementsByTagName("h_sols").item(0);
    			    	
    					NodeList solsMembers;
						try {
							solsMembers = sols.getElementsByTagName("member");
	    					for (int j = 0; j < solsMembers.getLength(); j++) {
	    	    				String solName = solsMembers.item(j).getTextContent();
	    	    				log.info("Adding solution [{}] to the solslist", solName);
		    					solsList.add(solName);
	    					}
						} catch (NullPointerException e) {
							log.info("Heuristic " + myName + "Contains an empty h_sols element.");
						}
    					
    					log.debug("Solslist for heuristic [{}] has [{}] elements", myName, solsList.size());

    					HeuristicResult myResult = new HeuristicResult(myName, importance, matched, action, header, summary, solsList);
    					
    					//TODO: Combine this and myResults. Probably by moving a lot of the functionaliity here inside of Results
    					log.debug("Checking match against Test Result [{}] Importance [{}]", selectedResultStatus, selectedImportance);
    					// Only add it to the output list if it matches both the primary tab and the importance tab that are selected.
    					if (matched == selectedResultStatus && importance.equals(selectedImportance)) {
    						log.debug("Test matches selected criteria adding to results list.");
        					tests.add(myResult);    						
    					}
    					
    					log.debug("Just submitted name, importance, matched, action, header and summary [{}] [{}] [{}] [{}] [{}] [{}] to the Results object.", myName, importance, matched, action, header, summary);
    					myResults.addResult(myName, importance, matched, action, header, summary);
    				  
    				}
    			}
    			
    			//TODO: Consolidate show tab and show link ui directives?
    			log.debug("Selected contains Main [{}] Primary [{}] Importance [{}]", selectedMainNavCategory, selectedResultStatus, selectedImportance);
    			// Loop through the main navigation types and hide or show the appropriate tab.
    			for (String s : MainNavTabs) {
    				String showTabString = "mainnav" + s + "tabshow";
       	        	String showLiString = "mainnav" + s + "lishow";
       	        	if (s.equals(selectedMainNavCategory)) {
       	        		log.info("Setting [{}] to show with [{}]", s, showTabString);
       	        		uidirectives.put(showTabString, "ui-tabs-show");
       	        		uidirectives.put(showLiString, "ui-tabs-selected ui-state-active");       	        		
       	        	} else {
       	        		log.info("Setting [{}] to hidden with [{}]", s, showTabString);
       	        		uidirectives.put(showTabString, "ui-tabs-hide");
       	        		uidirectives.put(showLiString, "ui-state-inactive");       	        		
       	        	}
    			}

    			// Loop through matched/unmatched and hide or show the appropriate tab.
       	        for (String s : primaryTabs) {
       	        	String showTabString = "pri" + s + "tabshow";
       	        	String showLiString = "pri" + s + "lishow";
       	        	if (selectedResultStatus == Boolean.parseBoolean(s)) {
       	        		log.info("Setting [{}] to show with [{}]", s, showTabString);
       	        		uidirectives.put(showTabString, "ui-tabs-show");
       	        		uidirectives.put(showLiString, "ui-tabs-selected ui-state-active");       	        		
       	        	} else {
       	        		log.info("Setting [{}] to hidden with [{}]", s, showTabString);
       	        		uidirectives.put(showTabString, "ui-tabs-hide");
       	        		uidirectives.put(showLiString, "ui-state-inactive");       	        		
       	        	}
       	        }
       	        
       	        // Loop through the importance levels and hide or show the appropriate tab.
       	        for (String s : importanceLevels) {
       	        	String showTabString = s + "tabshow";
       	        	String showLiString = s + "lishow";
       	        	if (!s.equals(selectedImportance)) {
       	        		log.info("Setting [{}] to show with [{}]", s, showTabString);
       	        		uidirectives.put(showTabString, "ui-tabs-hide");
       	        		uidirectives.put(showLiString, "ui-state-inactive");
       	        	} else {
       	        		log.info("Setting [{}] to hidden with [{}]", s, showTabString);
       	        		uidirectives.put(showTabString, "ui-tabs-show");
       	        		uidirectives.put(showLiString, "ui-tabs-selected ui-state-active");
       	        	}
       	        }
       	        
       	        // Set the numbers to display in the importance tabs
       	        uidirectives.put("showResults", selectedImportance);
	       	     if (selectedResultStatus) {
	             	messages.put("numCrit", Integer.toString(myResults.getNumFailedCrit()));
	             	messages.put("numHigh", Integer.toString(myResults.getNumFailedHigh()));
	             	messages.put("numMed", Integer.toString(myResults.getNumFailedMed()));
	             	messages.put("numLow", Integer.toString(myResults.getNumFailedLow()));        		
	         	} else {
	         		messages.put("numCrit", Integer.toString(myResults.getNumPassedCrit()));
	             	messages.put("numHigh", Integer.toString(myResults.getNumPassedHigh()));
	             	messages.put("numMed", Integer.toString(myResults.getNumPassedMed()));
	             	messages.put("numLow", Integer.toString(myResults.getNumPassedLow()));
	         	}
       	        
        	} catch (DOMException e) {
        		PDTViewExceptionHandler exceptionHandler = new PDTViewExceptionHandler(e);
    			exceptionHandler.processException();
//    		} catch (XPathExpressionException e) {
//    			PDTViewExceptionHandler exceptionHandler = new PDTViewExceptionHandler(e);
//    			exceptionHandler.processException();
//    		} catch (ParserConfigurationException e) {
//    			PDTViewExceptionHandler exceptionHandler = new PDTViewExceptionHandler(e);
//    			exceptionHandler.processException();
//    		} catch (SAXException e) {
//    			PDTViewExceptionHandler exceptionHandler = new PDTViewExceptionHandler(e);
//    			exceptionHandler.processException();
//    		} catch (IOException e) {
//    			PDTViewExceptionHandler exceptionHandler = new PDTViewExceptionHandler(e);
//    			exceptionHandler.processException();
    		}
        	
        	log.info("Selected criteria returned [{}] test results", tests.size());
        	// let's set some parameters which will be sent back to the jsp.

        	// The results of the tabs appropriate to said tab selection.
        	request.setAttribute("listTests",tests);
        	
        	// The summary numbers of the analysis
        	messages.put("numPassed", Integer.toString(myResults.getNumPassedTests()));
        	messages.put("numFailed", Integer.toString(myResults.getNumFailedTests()));
        	
        }
    	// Set which tabs should be active in the gui
    	messages.put("activeMainNavTab", activeMainNavTab);
    	messages.put("activePrimaryTab", activePrimaryTab);
    	messages.put("activeTab", activeTab);
    	
        // And finally send the request back to the jsp with all the info it needs to deal with it.
        request.getRequestDispatcher(activeJSPPage).forward(request, response);
    }
}