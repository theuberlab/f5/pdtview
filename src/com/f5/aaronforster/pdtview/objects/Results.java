package com.f5.aaronforster.pdtview.objects;

/**
 * Results is going to hold some statistics about the xml that i'm dealing with. I'm not sure if I'm going to use it long term.
 * 
 * @author forster
 *
 */
public class Results {
	/*
	 * Variables
	 */
	/**
	 * Enum the criticality levels so I can use them in a switch in a java 6 friendly way.
	 * @author forster
	 *
	 */
	private enum Levels {
	    critical, high, medium, low;
	}
	/**
	 * Holds the number of tests passed
	 */
	private int numPassedTests = 0;
	/**
	 * Holds the number of tests failed
	 */
	private int numFailedTests = 0;
	/**
	 * Holds the number of critical tests failed
	 */
	private int numFailedCrit = 0;
	/**
	 * Holds the number of high tests failed
	 */
	private int numFailedHigh = 0;
	/**
	 * Holds the number of medium tests failed
	 */
	private int numFailedMed = 0;
	/**
	 * Holds the number of low tests failed
	 */
	private int numFailedLow = 0;
	/*The numLevelpassed vars are not yet implemented*/
	/**
	 * Holds the number of critical tests passed
	 * The numLevelpassed vars are not yet implemented
	 */
	private int numPassedCrit = 0;
	/**
	 * Holds the number of high tests passed
	 * The numLevelpassed vars are not yet implemented
	 */
	private int numPassedHigh = 0;
	/**
	 * Holds the number of medium tests passed
	 * The numLevelpassed vars are not yet implemented
	 */
	private int numPassedMed = 0;
	/**
	 * Holds the number of low tests passed
	 * The numLevelpassed vars are not yet implemented
	 */
	private int numPassedLow = 0;
	/**
	 * The host name of course
	 */
	private String hostname;
	/**
	 * The platform ID like Z100 etc.
	 */
	private String platform;
	/**
	 * The serial number of the device.
	 */
	private String serialNumber;
	/**
	 * 11.2.1, 10.2.4, etc.
	 */
	private String version;
	/**
	 * Not sure what the difference is yet but it's there.
	 */
	private String fulldot;
	/**
	 * Says BIGIP. Not sure if there are other options.
	 */
	private String product;
	/**
	 * Build date or build nuber?
	 */
	private String built;
	/**
	 * Says Full in the demo results I'm working with.
	 */
	private String edition;
	/**
	 * I can only assume this is a chksum of the results or of the qkview itself?
	 */
	private String sha1;
	/*
	 * Constructors
	 */
    /**
     * The non-default default constructor.
     */
	public Results() {
    	
    }
	/**
     * Really this is just a sample.
     * 
     * @param name
     * @param nodeType
     */
    public Results(String name, int nodeType) {
    	
    }
    
    /*
     * Getters and setters
     */
    
    /*#######################################
     *   Getters
     * #####################################  
     */
    
    /**
     * Returns the number of results. 
     * 
     * @return
     */
    public int getNumResults() {
    	return numPassedTests + numFailedTests;
    }
    
    /**
     * Returns the number of tests passed.
     * @return
     */
    public int getNumPassedTests() {
    	return numPassedTests;
    }
    
    /**
     * Returns the number of tests failed.
     * @return
     */
    public int getNumFailedTests() {
    	return numFailedTests;
    }

    /**
     * Returns the number of tests of critical level importance that where identified.
     * @return
     */
    public int getNumFailedCrit() {
    	return numFailedCrit;
    }
    
    /**
     * Returns the number of tests of high level importance that where identified.
     * @return
     */
    public int getNumFailedHigh() {
    	return numFailedHigh;
    }
    
    /**
     * Returns the number of tests of medium level importance that where identified.
     * @return
     */
    public int getNumFailedMed() {
    	return numFailedMed;
    }
    
    /**
     * Returns the number of tests of low level importance that where identified.
     * @return
     */
    public int getNumFailedLow() {
    	return numFailedLow;
    }
    
    /**
     * Returns the number of tests of critical level importance that where not identified.
     * @return
     */
    public int getNumPassedCrit() {
    	return numPassedCrit;
    }
    
    /**
     * Returns the number of tests of high level importance that where not identified.
     * @return
     */
    public int getNumPassedHigh() {
    	return numPassedHigh;
    }
    
    /**
     * Returns the number of tests of medium level importance that where not identified.
     * @return
     */
    public int getNumPassedMed() {
    	return numPassedMed;
    }
    
    /**
     * Returns the number of tests of low level importance that where not identified.
     * @return
     */
    public int getNumPassedLow() {
    	return numFailedLow;
    }
    
    /**
     * Get's the host name.
     * @return
     */
    public String getHostname() {
		return hostname;
	}
	
    /**
     * Get's the platform (Z100, etc)
     * @return
     */
	public String getPlatform() {
		return platform;
	}
	
	/**
	 * Get's the serial number.
	 * @return
	 */
	public String getSerialNumber() {
		return serialNumber;
	}
	
	/**
	 * Get's the version.
	 * @return
	 */
	public String getVersion() {
		return version;
	}
	
	/**
	 * Get's the full dot version.
	 * @return
	 */
	public String getFulldot() {
		return fulldot;
	}
	
	/**
	 * Get's the product (BIGIP, etc.)
	 * @return
	 */
	public String getProduct() {
		return product;
	}
	
	/**
	 * Get's the build date/number
	 * @return
	 */
	public String getBuilt() {
		return built;
	}
    
	/**
	 * Get's the edition
	 * @return
	 */
	public String getEdition() {
		return edition;
	}
	
	/**
	 * Get's the sha1 hash
	 * @return
	 */
	public String getSha1() {
		return sha1;
	}
    
    /*#######################################
     *   Setters
     * #####################################  
     */
    //TODO: Modify this so that it can just take the xml node object as an argument and pull out the data on it's own.
	/**
     * Add another result. This one without solutions. Long term it will need a bunch of arguments
     * 
     * @param resultname
     * @param passed
     */
    public void addResult(String resultname, String importance, boolean matched, String action, String header, String summary) {
    	Levels level = Levels.valueOf(importance);
    	
    	switch(level) {
    	    case critical:
    	    	if (matched) {
    	    		numFailedTests++;
    	    		numFailedCrit++;
    	    	} else {
    	    		numPassedTests++;
    	    		numPassedCrit++;
    	    	}
    	        break;
    	    case high:
    	    	if (matched) {
    	    		numFailedTests++;
    	    		numFailedHigh++;
    	    	} else {
    	    		numPassedTests++;
    	    		numPassedHigh++;
    	    	}
    	        break;
    	    case medium:
    	    	if (matched) {
    	    		numFailedTests++;
    	    		numFailedMed++;
    	    	} else {
    	    		numPassedTests++;
    	    		numPassedMed++;
    	    	}
    	        break;
    	    case low:
    	    	if (matched) {
    	    		numFailedTests++;
    	    		numFailedLow++;
    	    	} else {
    	    		numPassedTests++;
    	    		numPassedLow++;
    	    	}
    	        break;
    	}

    }
	/**
     * Add another result. Long term it will need a bunch of arguments
     * 
     * @param resultname
     * @param passed
     */
    public void addResult(String resultname, String importance, boolean matched, String[] Sols, String action, String header, String summary) {
    	Levels level = Levels.valueOf(importance);
    	
    	switch(level) {
    	    case critical:
    	    	if (matched) {
    	    		numFailedTests++;
    	    		numFailedCrit++;
    	    	} else {
    	    		numPassedTests++;
    	    		numPassedCrit++;
    	    	}
    	        break;
    	    case high:
    	    	if (matched) {
    	    		numFailedTests++;
    	    		numFailedHigh++;
    	    	} else {
    	    		numPassedTests++;
    	    		numPassedHigh++;
    	    	}
    	        break;
    	    case medium:
    	    	if (matched) {
    	    		numFailedTests++;
    	    		numFailedMed++;
    	    	} else {
    	    		numPassedTests++;
    	    		numPassedMed++;
    	    	}
    	        break;
    	    case low:
    	    	if (matched) {
    	    		numFailedTests++;
    	    		numFailedLow++;
    	    	} else {
    	    		numPassedTests++;
    	    		numPassedLow++;
    	    	}
    	        break;
    	}
    	
    	
    	
    	
//    	<diagnostic name="H739986">
//    	    <run_data>
//    	      <h_importance>high</h_importance>
//    	      <match>false</match>
//    	    </run_data>
//    	    <results>
//    	      <h_sols>
//    	        <member>SOL10803</member>
//    	      </h_sols>
//    	      <h_name>H739986</h_name>
//    	      <h_action>Allocate additional memory to the XML processing engine using the 'add_del_internal' command. For more information, refer to the linked Solution.</h_action>
//    	      <h_header>Log messages report that the XML processing engine exceeded allocated memory</h_header>
//    	      <h_summary>Log messages report that the BIG-IP ASM XML processing engine exceeded allocated memory. By default, BIG-IP ASM allocates 300MB for XML processing. If the system is configured with large or numerous XML schemas, the XML processing engine may run out of memory. When this occurs, the BIG-IP ASM module is unable to process XML requests. As a result, XML requests may trigger metacharacter violations.</h_summary>
//    	    </results>
//    	  </diagnostic>
    
    
    }
    
    /**
     * Set's the hostname
     * @return
     */
	public void setHostname(String hostname) {
		this.hostname = hostname;
	}
	
	/**
	 * Set's the platform (Z100, etc)
	 * @param platform
	 */
	public void setPlatform(String platform) {
		this.platform = platform;
	}
	
	/**
	 * Set's the serial number.
	 * @param serialNumber
	 */
	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}
	
	/**
	 * Set's the version.
	 * @param version
	 */
	public void setVersion(String version) {
		this.version = version;
	}
	
	/**
	 * Set's the full dot version
	 * @param fulldot
	 */
	public void setFulldot(String fulldot) {
		this.fulldot = fulldot;
	}
	
	/**
	 * Set's the product (BIGIP, etc.)
	 * @param product
	 */
	public void setProduct(String product) {
		this.product = product;
	}
	
	/**
	 * Set's the build date/number
	 * @param built
	 */
	public void setBuilt(String built) {
		this.built = built;
	}
	
	/**
	 * Set's the edition
	 * @param edition
	 */
	public void setEdition(String edition) {
		this.edition = edition;
	}
	
	/**
	 * Set's the sha1 hash
	 * @param sha1
	 */
	public void setSha1(String sha1) {
		this.sha1 = sha1;
	}    

}
