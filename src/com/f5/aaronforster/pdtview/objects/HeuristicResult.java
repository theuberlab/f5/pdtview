package com.f5.aaronforster.pdtview.objects;

import java.util.List;

public class HeuristicResult {
	private String name;
	private String importance;
	private boolean matched;
	private String action;
	private String header;
	private String summary;
	private List<String> solutions;
	
	public HeuristicResult(String resultname, String importance, boolean matched, String action, String header, String summary, List<String> solutions) {
		this.name = resultname;
		this.importance = importance;
		this.matched = matched;
		this.action = action;
		this.header = header;
		this.summary = summary;
		this.solutions = solutions;
	}
	
	
	public String getName() {
		return name;
	}
	
	public String getImportance() {
		return importance;
	}
	
	public boolean getMatched() {
		return matched;
	}
	
	public String getAction() {
		return action;
	}
	
	public String getHeader() {
		return header;
	}
	public String getSummary() {
		return summary;
	}
	public List<String> getSolutions() {
		return solutions;
	}
}
