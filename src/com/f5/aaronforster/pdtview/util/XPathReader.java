package com.f5.aaronforster.pdtview.util;

import java.io.IOException;
import java.io.StringReader;

import javax.xml.XMLConstants;
import javax.xml.namespace.QName;
import javax.xml.parsers.*;
import javax.xml.xpath.*;

import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
 
public class XPathReader {
	private String xmlFile;
	private Document xmlDocument;
	private XPath xPath;
	private InputSource is;
	private String xmlString;
	
	public XPathReader(String xmlData) {
//		this.is = new InputSource(new StringReader(xmlData));
		this.xmlString = xmlData;
		initObjects();
	}
	
	
	
//  InputSource is = new InputSource(new StringReader(PDTResultsXML));
//	Document doc = dBuilder.parse(is);
	
	//Some experimenting I was doing with xpath to simplify/improve things. I'm back to the hard way for now.
//  XPathFactory xpf = XPathFactory.newInstance();
//  XPath xpath = xpf.newXPath();
//  xpath.evaluate("/diagnostic_output/system_information/hostname", is, XPathConstants.NODE);
	
	
	private void initObjects(){
//      try {
//        xmlDocument = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(xmlFile);
        xPath =  XPathFactory.newInstance().newXPath();
//      } catch (IOException ex) {
//        ex.printStackTrace();
//      } catch (SAXException ex) {
//        ex.printStackTrace();
//      } catch (ParserConfigurationException ex) {
//        ex.printStackTrace();
//      }
	}
 
	public Object read(String expression, QName returnType){
		try {
			is = new InputSource(new StringReader(xmlString));
//			return xPath.evaluate(expression, is, returnType);
			XPathExpression xPathExpression = xPath.compile(expression);
			return xPathExpression.evaluate  (is, returnType);
//			return xPathExpression.evaluate  (xmlDocument, returnType);
		} catch (XPathExpressionException ex) {
			ex.printStackTrace();
			return null;
		}
	}
}